/* A javascript-enhanced crossword puzzle */
(function ($, Drupal, drupalSettings) {
    'use strict';

    Drupal.behaviors.game_sorting = {
      attach: function (context, settings) {
        if (context == document) {
          var datas = drupalSettings.gameBoxes.sorting_game;
          $.each(datas, function( index, data ) {
            var uniqueid = data['uniqueID'];
            $(`#${uniqueid}`, context).sortablegame(data);
          });
        }
        
    }
};
    
})(jQuery, Drupal, drupalSettings)

