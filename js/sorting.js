(function($){
  $.fn.sortablegame = function(params){
    params = $.extend({
      cards: [],
      dragClass : 'dg',
      dropClass : 'dz',
      limit : 4
    }, params);

    var shuffle = function(array){
      var counter = array.length, temp, index;
      // While there are elements in the array
      while (counter > 0) {
        // Pick a random index
        index = Math.floor(Math.random() * counter);
        // Decrease counter by 1
        counter--;
        // And swap the last element with it
        temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
      }

      array = array.slice(0, params.limit);
      return array;
    }

    function build(el, cards) {
      var classes = shuffle(['top right','top left', 'bottom right', 'bottom left']);
      var dropClass = ['tc','bc','xl','xr'];
      for(var i in shuffle(cards)){
        var item = cards[i];
        el.append('<div data-target="'+item.id+'" class="'+classes.pop() + ' ' + params.dropClass +'"><img src="'+ item.drag +'" alt="'+ item.drag_img_title +'" /></div>');
        el.append('<div data-target="'+item.id+'" class="'+dropClass.pop() + ' ' + params.dragClass +'"><img src="'+item.drop+'" alt="'+ item.drop_img_title +'" /></div>');
      }
    }

    function revert(e){
      if(e){
        $(this).effect( "shake", {
          times: 3,
          distance: 10
        });
        $(this).delay(300);
      }
      return true;
    }

    function bind(el){
      el.find('.dg').each(function(){
        $(this).draggable( {
          containment: el,
          stack: el.find('.dz'),
          cursor: 'move',
          revert: revert,
          revertDuration: 500
        });
      });

      el.find('.dz').each(function(){
        $(this).droppable( {
          accept: el.find('.dg'),
          hoverClass: 'hovered',
          drop: handle,
          revertDuration : 300,
        });
      });
    }

    function handle(e, ui){
      var correctCards = $(this).closest('.game').data('answers');
      var slotNumber = $(this).data('target');
      var cardNumber = ui.draggable.data( 'target' );
      ui.draggable.position( { of: $(this), my: 'left top', at: 'left top' } );
      if ( slotNumber == cardNumber ) {
        ui.draggable.addClass( 'correct' );
        $(this).addClass( 'correct' );
        ui.draggable.draggable( 'disable' );
        // $(this).droppable( 'disable' );
        ui.draggable.draggable( 'option', 'revert', false );
        correctCards++;
        $(e.target).closest('.game').data('answers',correctCards);
      }

      // If all the cards have been placed correctly then display a message
      // and reset the cards for another go
      if ( correctCards == params.limit ) {
        const cta = $(`._${params.uniqueID}_button` );
        const replay = $( `[data-game="#${params.uniqueID}"]` );
        setTimeout(() => {
          replay.prop("disabled", false ).removeClass('btn-disabled');
          $(`#${params.uniqueID}`).addClass('hidden');
          $(`#${params.uniqueID}_success`).removeClass('hidden');
          cta.removeAttr('disabled').removeClass('btn-disabled');
        }, 500);
      }
    }
    var setSuccessImage = function(el){
      var img = `<div id="${params.uniqueID}_success" class="success hidden"><img src="${params.img_url.url}"  alt="${params.img_url.alt}"/></div>`;
      el.after(img);
    }
    var init = function(el){
      build(el, params.cards);
      setSuccessImage(el);
      setTimeout(function(){
        bind(el);
        $(document).on("click",".sorting_replay",resetGamePlay);
      },3000);
    }
    var resetGamePlay = function(){
      let game = $($(this).data('game'));
      const options = game.data('options');
      const replay = $( `[data-game="#${options.id}"]` );
      game.removeClass('hidden');
      game.next().addClass('hidden');
      game.data('answers',0).html('');
      replay.prop("disabled", true ).addClass('btn-disabled');
      build(game, options.cards);
      bind(game);
    }
    $(this).each(function() {
      $(this).data('options',params).data('answers', 0);
      init($(this));
    });

    return $(this);
  }
})(jQuery);
